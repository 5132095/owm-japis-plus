#OWM JAPIs plus

fork OWM_JAPIs 2.5  with the addition of the possibility of obtaining the weather for a specified period of time

* Modified net/aksingh/java/api/owm/OpenWeatherMap.java
* Added by/mercom/java/api/owm/HistoricalWeather.java

### Example: ###

```
#!java

 import by.mercom.java.api.owm.HistoricalWeather;
 import net.aksingh.java.api.owm.CurrentWeatherData;
 
 import java.util.ArrayList;
 import java.util.Date;
 
 public class Main {
     public static void main(String[] args) {
         HistoricalWeather hw = new HistoricalWeather("api key");
         ArrayList<CurrentWeatherData> cwd_array = hw.getHistoryWeatherByCityCode(12341234, new Date(), new Date());
         for (int i = 0; i < cwd_array.size(); i++) {
             cwd_array.get(i).getDateTime();
             cwd_array.get(i).getMainData_Object().getTemperature();
             cwd_array.get(i).getMainData_Object().getMaxTemperature();
         }
     }
 }
```

cwd_array.get(i) -- see CurrentWeatherData in OWM_JAPIs 2.5 (CurrentWeatherData.html in java doc)