package by.mercom.java.api.owm;

import net.aksingh.java.api.owm.CurrentWeatherData;
import net.aksingh.java.api.owm.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by dm13y on 11/24/14.
 */
public class HistoricalWeather {
    ArrayList<CurrentWeatherData> weatherArray;
    private String apiKey;

    /**
     * @param apiKey   key for getting data on OpenWeatherMap
     */
    public HistoricalWeather(String apiKey) {
        this.apiKey = apiKey;
    }

    /**
     * convert date.time() to unix time
     * @param date
     * @return UTC time
     */
    private long getUTCTime(Date date) {
        return date.getTime() / 1000;
    }

    /**
     * getting historical weather by cityCode
     * @param cityCode
     * @param start beginning of the history of weather
     * @param end end of story weather
     * @return array consisting of the values of the weather during this time period
     */
    public ArrayList<CurrentWeatherData> getHistoryWeatherByCityCode(long cityCode, Date start, Date end) {
        weatherArray = new ArrayList<CurrentWeatherData>();
        OpenWeatherMap owm = new OpenWeatherMap(apiKey);
        try {
            String historicalJSON_Date = owm.getHistoricalJSONWeatherByCityCode(cityCode, getUTCTime(start), getUTCTime(end));
            JSONArray jsonArray = new JSONObject(historicalJSON_Date).getJSONArray("list");
            for (int i = 0; i < jsonArray.length(); i++) {
                weatherArray.add(new CurrentWeatherData(jsonArray.getJSONObject(i)));
            }
        } catch (IOException io_ex) {
            io_ex.printStackTrace();
        } catch (JSONException json_ex) {
            json_ex.printStackTrace();
        }
        return weatherArray;
    }

    public ArrayList<CurrentWeatherData> getHistoryWeatherByCityName(String cityName, Date start, Date end) {
        weatherArray = new ArrayList<CurrentWeatherData>();
        OpenWeatherMap owm = new OpenWeatherMap(apiKey);
        try {
            String historicalJSON_Date = owm.getHistoricalJSONWeatherByCityName(cityName, getUTCTime(start), getUTCTime(end));
            JSONArray jsonArray = new JSONObject(historicalJSON_Date).getJSONArray("list");
            for (int i = 0; i < jsonArray.length(); i++) {
                weatherArray.add(new CurrentWeatherData(jsonArray.getJSONObject(i)));
            }
        } catch (IOException io_ex) {
            io_ex.printStackTrace();
        } catch (JSONException json_ex) {
            json_ex.printStackTrace();
        }
        return weatherArray;
    }

}
